js.amstock
**********

Introduction
============

This library packages `amCharts`_ for `fanstatic`_. It provides the `Stock`_
chart libray, which also includes the normal `Chart`_ library.

.. _fanstatic: http://fanstatic.org
.. _amCharts: http://www.amcharts.com
.. _Stock: http://www.amcharts.com/stock/
.. _Chart: http://www.amcharts.com/javascript/

This requires integration between your web framework and ``fanstatic``, and
making sure that the original resources (shipped in the ``resources`` directory
in ``js.amstock``) are published to some URL.
